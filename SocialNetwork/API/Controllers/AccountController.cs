﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web.Http;
using API.Models;
using API.Models.UserModels;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.UserDTOs;
using Microsoft.AspNet.Identity;

namespace API.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly IUserService _userService;
        private readonly MapperConfiguration _mapperConfig;

        public AccountController(IUserService userService)
        {
            _userService = userService;
            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDto, FullUserModel>();
                cfg.CreateMap<UserModel, UserDto>();
            });
        }

        /// <summary>
        /// Return logged user's profile
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [Route("profile")]
        [HttpGet]
        public async Task<IHttpActionResult> Profile()
        {
            try
            {
                var uId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var user = await _userService.FindByIdAsync(uId);
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<UserDto, FullUserModel>(user);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // POST api/Account/Register
        /// <summary>
        /// Sign up
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var account = new AccountDto() {UserName = userModel.UserName, Password = userModel.Password};

            var mapper = _mapperConfig.CreateMapper();
            var user = mapper.Map<UserModel, UserDto>(userModel);

            var result = await _userService.CreateAsync(account, user);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("errors", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
