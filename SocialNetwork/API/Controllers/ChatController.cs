﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using API.Hubs;
using API.Models;
using API.Models.ChatModels;
using API.Models.MessageModels;
using API.Models.UserModels;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.ChatDTOs;
using BLL.Interfaces.DTOs.MessageDTOs;
using BLL.Interfaces.DTOs.UserDTOs;
using DatabaseProvider.Entity;
using Temperature = API.Models.ChatModels.Temperature;

namespace API.Controllers
{
    [RoutePrefix("api/chats")]
    public class ChatController : ApiController
    {
        private readonly IChatService _chatService;
        private readonly MapperConfiguration _mapperConfig;
        private readonly IUserService _userService;

        public ChatController(IChatService chatService, IUserService userService)
        {
            _chatService = chatService;
            _userService = userService;

            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDto, ShortUserModel>();
                cfg.CreateMap<ShortUserDto, ShortUserModel>();
                cfg.CreateMap<MessageDto, MessageModel>();
                cfg.CreateMap<ChatDto, ChatModel>();
                cfg.CreateMap<BLL.Interfaces.Temperature, Models.ChatModels.Temperature>();
            });
        }

        /// <summary>
        /// Return all users chats
        /// </summary>
        /// <param name="criteria">Chat name or member name</param>
        /// <param name="take">paging</param>
        /// <param name="skip">paging</param>
        /// <returns>Array of ChatModel</returns>
        [Authorize]
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get([FromUri]string criteria = null, [FromUri]int take=0, [FromUri]int skip=0)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                IEnumerable<ChatDto> chats;
                if (take == 0)
                {
                    chats = criteria !=null ? await _chatService.FindByNameOrMemberAsync(userId, criteria)
                        : await _chatService.GetAllUserChatsAsync(userId);
                }
                else
                {
                    chats = criteria != null ? await _chatService.FindByNameOrMemberPagedAsync(userId, criteria, take, skip)
                        : await _chatService.GetUserChatsPagedAsync(userId, take, skip);
                }
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<IEnumerable<ChatDto>, IEnumerable<ChatModel>>(chats);
                var chatModels = result as IList<ChatModel> ?? result.ToList();
                foreach (var model in chatModels)
                {
                    var temp = await _chatService.GetTemperatureAsync(model.Id);
                    model.Temperature = mapper.Map<BLL.Interfaces.Temperature, Temperature>(temp);
                }
                return Ok(chatModels);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Returns chat by id
        /// </summary>
        /// <param name="chatId">Chat id</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [Authorize]
        [HttpGet]
        [Route("{chatId:int}")]
        public async Task<IHttpActionResult> GetChat(int chatId)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var chat = await _chatService.GetByIdAsync(chatId, userId);
                if(chat==null)
                    throw new Exception("Chat not found");
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<ChatDto, ChatModel>(chat);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Return list of messeges from chat
        /// </summary>
        /// <param name="chatId">Chat id</param>
        /// <param name="minTime">Minimum sent message time</param>
        /// <param name="take">paging</param>
        /// <param name="skip">paging</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("{chatId:int}/messages")]
        public async Task<IHttpActionResult> GetMessages(int chatId, [FromUri]DateTime? minTime=null, [FromUri]int take=0, [FromUri]int skip=0)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                IEnumerable<MessageDto> messages;
                if (take == 0)
                {
                    messages = await _chatService.GetMessagesAsync(chatId, userId);
                }
                else
                {
                    messages = await _chatService.GetMessagesPagedAsync(chatId, userId, skip, take);
                }
                if (minTime != null)
                {
                    messages = messages.Where(m => m.Time > minTime);
                }
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<IEnumerable<MessageDto>, IEnumerable<MessageModel>>(messages);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Return chat members
        /// </summary>
        /// <param name="chatId">Chat id</param>
        /// <returns>Array of Users</returns>
        [Authorize]
        [HttpGet]
        [Route("{chatId:int}/members")]
        public async Task<IHttpActionResult> GetMembers(int chatId)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var members = await _chatService.GetMembersAsync(chatId, userId);
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<IEnumerable<ShortUserDto>, IEnumerable<ShortUserDto>>(members);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Create chat
        /// </summary>
        /// <param name="chat">Chat model</param>
        /// <returns>Id of created chat</returns>
        [Authorize]
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> CreateChat([FromBody]CreateChatModel chat)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var chatDto = new ChatDto()
                {
                    Name = chat.Name,
                    Creator = new UserDto() {Id = userId},
                    IsDialog = chat.IsDialog
                    
                };
                var chatId = await _chatService.CreateChat(chatDto, chat.MembersIds);
                var resultDto = await _chatService.GetByIdAsync(chatId, userId);
                var mapper = _mapperConfig.CreateMapper();
                var resultModel = mapper.Map<ChatDto, ChatModel>(resultDto);
                var members = await _chatService.GetMembersAsync(chatId, userId);
                foreach (var member in members)
                {
                    ChatHub.InviteToChatStatic(member.Id, resultModel);
                }
                
                return Ok(chatId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        /// <summary>
        /// Ivite user to chat
        /// </summary>
        /// <param name="chatId">Chat id</param>
        /// <param name="userId">User id</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("{chatId:int}/members/{userId:int}")]
        public async Task<IHttpActionResult> InviteUser(int chatId, int userId)
        {
            try
            {
                var currentUserId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                await _chatService.AddUserToChat(chatId, userId, currentUserId);
                var chat = await _chatService.GetByIdAsync(chatId, currentUserId);
                var mapper = _mapperConfig.CreateMapper();
                var chatModel = mapper.Map<ChatDto, ChatModel>(chat);
                ChatHub.InviteToChatStatic(userId, chatModel);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Dismember user from chat if you are chat creator.
        /// Leave chat 
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="chatId">Chat id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{chatId:int}/members/{userId:int}")]
        public async Task<IHttpActionResult> DismemberUser(int userId, int chatId)
        {
            try
            {
                var currentUserId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                await _chatService.DeleteUserFromChat(chatId, userId, currentUserId);
                ChatHub.UnsubscribeStatic(userId, chatId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Delete history of user's chat
        /// </summary>
        /// <param name="chatId">Chat id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{chatId:int}")]
        public async Task<IHttpActionResult> DeleteChatHistory(int chatId)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                await _chatService.DeleteChat(chatId, userId);
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}