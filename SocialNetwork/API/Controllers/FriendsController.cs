﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using API.Models;
using API.Models.UserModels;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.UserDTOs;
using DatabaseProvider.Entity;
using Microsoft.AspNet.Identity;

namespace API.Controllers
{
    [RoutePrefix("api/Friends")]
    public class FriendsController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IFriendService _friendService;

        public FriendsController(IUserService userService, IFriendService friendService)
        {
            _userService = userService;
            _friendService = friendService;
        }

        /// <summary>
        /// Return user's friends
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetFriends()
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var friends = await _friendService.GetAllFriendsAsync(userId);
                Mapper.Initialize(cfg => cfg.CreateMap<UserDto, ShortUserModel>());
                var result = Mapper.Map<IEnumerable<UserDto>, IEnumerable<ShortUserModel>>(friends);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Add user to friends
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("{userId:int}")]
        public async Task<IHttpActionResult> AddToFriends(int userId)
        {
            try
            {
                var currentUserId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                await _friendService.AddUserToFriendsAsync(currentUserId, userId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Remove user from friends
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        [Route("{userId:int}")]
        public async Task<IHttpActionResult> RemoveFromFriends(int userId)
        {
            try
            {
                var currentUserId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                await _friendService.RemoveUserFromFriendsAsync(currentUserId, userId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
