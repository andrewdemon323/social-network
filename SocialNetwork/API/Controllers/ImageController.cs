﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using BLL.Interfaces;
using BLL.Interfaces.DTOs;

namespace API.Controllers
{   
    [RoutePrefix("api/images")]
    public class ImageController : ApiController
    {
        private readonly IUserService _userService;

        public ImageController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Return user's avatar
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [Route("{id:int}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            string filePath = await _userService.GetImagePathAsync(id);
            if (filePath != null)
            {
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
                {
                    Image image = Image.FromStream(fileStream);
                    MemoryStream memoryStream = new MemoryStream();
                    image.Save(memoryStream, ImageFormat.Jpeg);
                    result.Content = new ByteArrayContent(memoryStream.ToArray());
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    return result;
                }
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }


        /// <summary>
        /// Upload avatar for user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Authorize]
        public async Task<HttpResponseMessage> Upload()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var httpRequest = HttpContext.Current.Request;
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 5; //Size = 5 MB  
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 5 mb.");
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            var filePath = HttpContext.Current.Server.MapPath($"~/App_Data/" + postedFile.FileName);
                            postedFile.SaveAs(filePath);
                            _userService.UpdateImagePath(userId, filePath);
                        }
                    }
                    var message1 = string.Format("Image Uploaded Successfully.");
                    return Request.CreateResponse(HttpStatusCode.Created, message1);
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format(ex.Message);
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }
    }
}
