﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using API.Hubs;
using API.Models;
using API.Models.MessageModels;
using API.Models.UserModels;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs;
using BLL.Interfaces.DTOs.MessageDTOs;
using BLL.Interfaces.DTOs.UserDTOs;
using DatabaseProvider.Entity;

namespace API.Controllers
{
    [RoutePrefix("api/messages")]
    public class MessageController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IMessageService _messageService;
        private readonly MapperConfiguration _mapperConfig;

        public MessageController(IUserService userService, IMessageService messageService)
        {
            _userService = userService;
            _messageService = messageService;
            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MessageDto, MessageModel>();
                cfg.CreateMap<UserDetails, UserDto>();
                cfg.CreateMap<UserDto, ShortUserModel>();
            });
        }

        /// <summary>
        /// Send message to user or to chat
        /// </summary>
        /// <param name="msgModel">Message to send</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Send([FromBody] SendMessageModel msgModel)
        {
            if (msgModel == null || msgModel.Content?.Length==0)
                return BadRequest("empty message");
            var senderId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
            if (msgModel.ReceiverId != 0)
            {
                try
                {
                    Mapper.Initialize(cfg => cfg.CreateMap<SendMessageModel, SendMessageDto>()
                        .ForMember("SenderId", opt => opt.UseValue(senderId)));
                    var msg = Mapper.Map<SendMessageModel, SendMessageDto>(msgModel);
                    var id = await _messageService.SendMessageToUser(msg);
                    if (id != 0)
                    {
                        var mes = await _messageService.GetById(id, senderId);
                        var res = _mapperConfig.CreateMapper().Map<MessageDto, MessageModel>(mes);
                        ChatHub.SendMsg(res, mes.ChatId);
                    }
                    return Ok(id);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            if (msgModel.ChatId != 0)
            {
                try
                {
                    Mapper.Initialize(cfg => cfg.CreateMap<SendMessageModel, SendMessageDto>()
                        .ForMember("SenderId", opt => opt.UseValue(senderId)));
                    var msg = Mapper.Map<SendMessageModel, SendMessageDto>(msgModel);
                    var id = await _messageService.SendMessageToChat(msg);
                    if (id != 0)
                    {
                        var mes = await _messageService.GetById(id, senderId);
                        var res = _mapperConfig.CreateMapper().Map<MessageDto, MessageModel>(mes);
                        ChatHub.SendMsg(res, mes.ChatId);
                    }
                    return Ok(id);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            return BadRequest("not set receiverId or chatId");
        }

        /// <summary>
        /// Remove message from chat history
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{messageId:int}")]
        public async Task<IHttpActionResult> DeleteMessage(int messageId)
        {
            try
            {
                var userId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                await _messageService.DeleteMessage(messageId, userId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
