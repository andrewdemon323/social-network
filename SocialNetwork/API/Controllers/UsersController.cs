﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using API.Models;
using API.Models.UserModels;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.UserDTOs;
using DatabaseProvider.Entity;

namespace API.Controllers
{
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;
        private readonly MapperConfiguration _mapperConfig;

        public UsersController(IUserService userService)
        {
            _userService = userService;
            _mapperConfig = new MapperConfiguration(cfg=> {
                cfg.CreateMap<UserDto, ShortUserModel>();
                cfg.CreateMap<UserDto, FullUserModel>();
            });

        }

        /// <summary>
        /// Return all users in network
        /// </summary>
        /// <param name="criteria">Search filter</param>
        /// <param name="take">paging</param>
        /// <param name="skip">paging</param>
        /// <returns></returns>
        [Authorize]
        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllUsers([FromUri]SearchCriteria criteria, [FromUri]int take=0, [FromUri]int skip=0)
        {
            try
            {
                IEnumerable<UserDto> users;
                if (criteria != null)
                {
                    users = await _userService.FindByCriteriaAsync(criteria, take, skip);
                }
                else
                {
                    users = await _userService.GetAllUsersAsync(take, skip);
                }
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<IEnumerable<UserDto>, IEnumerable<ShortUserModel>>(users);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Return individual user
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns></returns>
        [Authorize]
        [Route("{userId:int}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUser(int userId)
        {
            try
            {
                var user = await _userService.FindByIdAsync(userId);
                var mapper = _mapperConfig.CreateMapper();
                var result = mapper.Map<UserDto, FullUserModel>(user);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Return path from you to some user thow your friends
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns></returns>
        [Authorize]
        [Route("{userId:int}/path")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTheShortestPath(int userId)
        {
            try
            {
                var currentId = await _userService.GetUserIdByUserNameAsync(User.Identity.Name);
                var path = await _userService.GetTheShortestPath(currentId, userId);
                var mapper = _mapperConfig.CreateMapper();
                var result =  mapper.Map<List<UserDto>, List<ShortUserModel>>(path);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
