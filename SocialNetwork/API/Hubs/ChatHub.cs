﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using API.Models.ChatModels;
using API.Models.MessageModels;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;

namespace API.Hubs
{
    public class ChatHub : Hub
    {
        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

        private static Dictionary<int, string> _connections = new Dictionary<int, string>();

        public static void SendMsg(MessageModel msg, int chatId)
        {
            hubContext.Clients.Group(chatId.ToString()).addNewMessageToPage(msg);
        }

        /// <summary>
        /// Connect user
        /// </summary>
        /// <param name="userId">User id</param>
        public void Connect(int userId)
        {
            _connections[userId] = Context.ConnectionId;
            Groups.Add(Context.ConnectionId, $"user{userId}");
            Clients.All.updateStatus(userId, true);
        }

        /// <summary>
        /// Notify chat members about message typing
        /// </summary>
        /// <param name="userId">Id user who typing message</param>
        /// <param name="chatId">Id chat where user typing message</param>
        public void TypeMessage(int userId, int chatId)
        {
            Clients.Group(chatId.ToString()).notifyTyping(userId, chatId);
        }

        public void Disconnect(int userId)
        {
            if (!_connections.ContainsKey(userId)) return;
            _connections.Remove(userId);
            Clients.All.updateStatus(userId, false);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var userId = GetKeyByValue(Context.ConnectionId);
            if (userId != 0)
            {
                _connections.Remove(userId);
                Clients.All.updateStatus(userId, false);
            }

            return base.OnDisconnected(stopCalled);
        }

        public void SubscribeOnChat(int userId, int chatId)
        {
            SubscribeOnChatStatic(userId, chatId);
        }

        public static void SubscribeOnChatStatic(int userId, int chatId)
        {
            if (!_connections.ContainsKey(userId)) return;
            var userConn = _connections[userId];
            if (userConn == null) return;
            hubContext.Groups.Add(userConn, chatId.ToString());
        }

        public static void InviteToChatStatic(int userId, ChatModel chat)
        {
            if (!_connections.ContainsKey(userId)) return;
            var userConn = _connections[userId];
            if (userConn == null) return;
            hubContext.Clients.Client(userConn).addChatToPage(chat);
            SubscribeOnChatStatic(userId, chat.Id);
        }

        public static void UnsubscribeStatic(int userId, int chatId)
        {
            if (!_connections.ContainsKey(userId)) return;
            var userConn = _connections[userId];
            if (userConn == null) return;
            hubContext.Groups.Remove(userConn, chatId.ToString());
        }

        /// <summary>
        /// Update status user for other users
        /// </summary>
        /// <param name="userId">User id</param>
        public void UpdateStatus(int userId)
        {
            var status = _connections.ContainsKey(userId);
            Clients.Caller.updateStatus(userId, status);
        }

        private static int GetKeyByValue(string value)
        {
            return _connections.Where(conn => conn.Value == value).Select(conn => conn.Key).FirstOrDefault();
        }
    }
}