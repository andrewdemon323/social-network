﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API.Models.MessageModels;
using API.Models.UserModels;

namespace API.Models.ChatModels
{
    public class ChatModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDialog { get; set; }

        public bool Deleted { get; set; }

        public ShortUserModel Creator { get; set; }

        public DateTime CreateTime { get; set; }

        public MessageModel LastMessage { get; set; }

        public Temperature Temperature { get; set; }
    }
}