﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.ChatModels
{
    /// <summary>
    /// Chat model
    /// </summary>
    public class CreateChatModel
    {
        /// <summary>
        /// Chat naem
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Chat is dialog, or not
        /// </summary>
        public bool IsDialog { get; set; }

        /// <summary>
        /// Ids of users which will be invited in chat
        /// </summary>
        public List<int> MembersIds { get; set; }
    }
}