﻿namespace API.Models.ChatModels
{
    public enum Temperature
    {
        Frozen,
        Cold,
        Warm,
        Hot
    }
}