﻿using System;
using API.Models.UserModels;

namespace API.Models.MessageModels
{
    public class MessageModel
    {
        public int Id { get; set; }

        public int ChatId { get; set; }

        public ShortUserModel Sender { get; set; }

        public string Content { get; set; }
        
        public DateTime Time { get; set; }

    }
}