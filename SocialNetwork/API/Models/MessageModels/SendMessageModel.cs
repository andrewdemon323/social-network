﻿using System.ComponentModel.DataAnnotations;

namespace API.Models.MessageModels
{
    /// <summary>
    /// Model for sending message
    /// </summary>
    public class SendMessageModel
    {
        /// <summary>
        /// User's id whom messege will be sent (not reqired if ChatId presents)
        /// </summary>
        public int ReceiverId { get; set; }
        /// <summary>
        /// Id of chat where messege will be sent (not required if ReceiverId presents)
        /// </summary>
        public int ChatId { get; set; }
        /// <summary>
        /// Text of message
        /// </summary>
        [Required(ErrorMessage = "Message haven't be empty")]
        public string Content { get; set; }
    }
}