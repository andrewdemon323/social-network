﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DatabaseProvider.Entity;

namespace API.Models
{
    public class UserModel
    {
        public UserModel()
        {
            Gender = Gender.Unknown;
            DateOfBirth = null;
        }

        /// <summary>
        /// Login
        /// </summary>
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// Sex
        /// </summary>
        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { get; set; }

        /// <summary>
        /// Date of Birth
        /// </summary>
        [Display(Name = "DateOfBirth")]
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Confirm password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}