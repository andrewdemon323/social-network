﻿using System;
using DatabaseProvider.Entity;

namespace API.Models.UserModels
{
    public class FullUserModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? RegDate { get; set; }

        public DateTime? LastModif { get; set; }
    }
}