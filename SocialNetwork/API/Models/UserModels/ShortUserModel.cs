﻿using System;

namespace API.Models.UserModels
{
    public class ShortUserModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? LastModif { get; set; }
    }
}