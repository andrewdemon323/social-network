﻿using API.Providers;
using BLL.Interfaces;
using BLL.Services;
using DatabaseProvider;
using Microsoft.Practices.Unity;
using SqlDataProvider;

namespace API.Utils
{
    public class UnityContainerBuilder
    {
        public static IUnityContainer BuildUnityContainer(string connectionString)
        {
            var container = new UnityContainer();
            container.RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager(),
                new InjectionConstructor(connectionString));
            container.RegisterType<ApplicationDbContext>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IChatService, ChatService>();
            container.RegisterType<IMessageService, MessageService>();
            container.RegisterType<IFriendService, FriendService>();
            return container;
        }
    }
}