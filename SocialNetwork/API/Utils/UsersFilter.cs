﻿using DatabaseProvider.Entity;

namespace API.Utils
{
    public class UsersFilter
    {
        public string NameLike { get; set; }

        public int? MinAge { get; set; }

        public int? MaxAge { get; set; }

        public Gender? Gender { get; set; }
    }
}