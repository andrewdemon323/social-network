﻿using System;
using BLL.Interfaces.DTOs.MessageDTOs;
using BLL.Interfaces.DTOs.UserDTOs;

namespace BLL.Interfaces.DTOs.ChatDTOs
{
    public class ChatDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDialog { get; set; }

        public UserDto Creator { get; set; }

        public DateTime CreateTime { get; set; }

        public bool Deleted { get; set; }

        public MessageDto LastMessage { get; set; }
    }
}