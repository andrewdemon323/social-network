﻿using System;
using BLL.Interfaces.DTOs.UserDTOs;

namespace BLL.Interfaces.DTOs.MessageDTOs
{
    public class MessageDto
    {
        public int Id { get; set; }

        public int ChatId { get; set; }

        public UserDto Sender { get; set; }

        public string Content { get; set; }

        public DateTime Time { get; set; }
    }
}