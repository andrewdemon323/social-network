﻿namespace BLL.Interfaces.DTOs.MessageDTOs
{
    public class SendMessageDto
    {
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public int ChatId { get; set; }
        public string Content { get; set; }
    }
}