﻿namespace BLL.Interfaces.DTOs.UserDTOs
{
    public class AccountDto
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}