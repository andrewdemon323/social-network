﻿using System;

namespace BLL.Interfaces.DTOs.UserDTOs
{
    public class ShortUserDto
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? LastModif { get; set; }
    }
}