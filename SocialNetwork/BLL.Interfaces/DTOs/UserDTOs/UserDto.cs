﻿using System;
using DatabaseProvider.Entity;

namespace BLL.Interfaces.DTOs.UserDTOs
{
    public class UserDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string ImgPath { get; set; }

        public DateTime? RegDate { get; set; }

        public DateTime? LastModif { get; set; }
    }
}