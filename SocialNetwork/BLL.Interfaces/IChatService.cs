﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces.DTOs;
using BLL.Interfaces.DTOs.ChatDTOs;
using BLL.Interfaces.DTOs.MessageDTOs;
using BLL.Interfaces.DTOs.UserDTOs;

namespace BLL.Interfaces
{
    public interface IChatService : IDisposable
    {
        Task<IEnumerable<ChatDto>> GetAllUserChatsAsync(int userId);

        Task<IEnumerable<ChatDto>> GetUserChatsPagedAsync(int userId, int take, int skip);

        Task<ChatDto> GetByIdAsync(int chatId, int userId);

        Task<Temperature> GetTemperatureAsync(int id);

        Task<IEnumerable<MessageDto>> GetMessagesAsync(int chatId, int userId);

        Task<IEnumerable<MessageDto>> GetMessagesPagedAsync(int chatId, int userId, int take, int skip);

        Task<IEnumerable<ShortUserDto>> GetMembersAsync(int chatId, int userId);

        Task<int> CreateChat(ChatDto chatDto, IEnumerable<int> membersIds);

        Task<IEnumerable<ChatDto>> FindByNameOrMemberAsync(int userId, string criteria);

        Task<IEnumerable<ChatDto>> FindByNameOrMemberPagedAsync(int userId, string criteria, int take, int skip);

        Task AddUserToChat(int chatId, int userId, int currentUserId);

        Task DeleteUserFromChat(int chatId, int userId, int currentUserId);

        Task DeleteChat(int chatId, int userId);
    }
}