﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces.DTOs.UserDTOs;

namespace BLL.Interfaces
{
    public interface IFriendService : IDisposable
    {
        Task<IEnumerable<UserDto>> GetAllFriendsAsync(int userId);
        Task AddUserToFriendsAsync(int currentUserId, int userId);
        Task RemoveUserFromFriendsAsync(int currentUserId, int userId);
    }
}