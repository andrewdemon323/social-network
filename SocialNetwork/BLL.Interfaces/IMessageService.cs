﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces.DTOs;
using BLL.Interfaces.DTOs.MessageDTOs;
using DatabaseProvider.Entity;

namespace BLL.Interfaces
{
    public interface IMessageService : IDisposable
    {
        IEnumerable<MessageDto> GetMessagesByChatId(int chatId);

        Task<int> SendMessageToUser(SendMessageDto message);

        Task<int> SendMessageToChat(SendMessageDto message);

        Task DeleteMessage(int messageId, int userId);

        Task<MessageDto> GetById(int id, int userId);
    }
}