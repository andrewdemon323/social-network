﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces.DTOs.UserDTOs;
using Microsoft.AspNet.Identity;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<UserDto> FindByIdAsync(int userId);

        Task<int> GetUserIdByUserNameAsync(string userName);

        Task<AccountDto> FindAsync(string userName, string password);
        
        Task<IdentityResult> CreateAsync(AccountDto account, UserDto userInfo);

        Task<IEnumerable<UserDto>> GetAllUsersAsync(int take, int skip);

        Task<IEnumerable<UserDto>> FindByCriteriaAsync(SearchCriteria criteria, int take, int skip);

        Task<string> GetImagePathAsync(int userId);

        Task<List<UserDto>> GetTheShortestPath(int fromUserId, int toUserId);

        void UpdateImagePath(int userId, string path);
    }
}