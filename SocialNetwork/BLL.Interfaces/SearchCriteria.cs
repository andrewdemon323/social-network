﻿using DatabaseProvider.Entity;

namespace BLL.Interfaces
{
    public class SearchCriteria
    {
        public string Name { get; set; }
        public Gender? Gender { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
    }
}