﻿namespace BLL.Interfaces
{
    public enum Temperature
    {
        Frozen,
        Cold,
        Warm,
        Hot
    }
}