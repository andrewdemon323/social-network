﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using DatabaseProvider;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SqlDataProvider;
using SqlDataProvider.Repositories;

namespace BLL.Services.Tests
{
    [TestClass]
    public class ChatServiceTests
    {
        private Mock<IUnitOfWork> _db;
        private Mock<Repository<ChatHistory>> _chatRepoMock;
        private ChatService _chatService;
        [TestMethod]
        public async Task TestMethod1()
        {
            //Arrange
            var fakeChats = new List<ChatHistory>
            {
                new ChatHistory{Id=1, ChatId = 1, UserId = 1, Deleted = false,
                    Chat = new Chat{Id=1, IsDialog = true, Name = "test1"}},
                new ChatHistory{Id=1, ChatId = 2, UserId = 1, Deleted = false,
                    Chat = new Chat{Id=2, IsDialog = false, Name = "test2"}},
                new ChatHistory{Id=1, ChatId = 3, UserId = 1, Deleted = false,
                    Chat = new Chat{Id=3, IsDialog = true, Name = "test3"}},
                new ChatHistory{Id=1, ChatId = 4, UserId = 2, Deleted = false,
                    Chat = new Chat{Id=4, IsDialog = false, Name = "test4"}},
                new ChatHistory{Id=1, ChatId = 5, UserId = 3, Deleted = false,
                    Chat = new Chat{Id=5, IsDialog = true, Name = "test5"}}
            }.AsQueryable();
            var mockSet = new Mock<DbSet<ChatHistory>>();
            mockSet.As<IDbAsyncEnumerable<ChatHistory>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<ChatHistory>(fakeChats.GetEnumerator()));

            mockSet.As<IQueryable<ChatHistory>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<ChatHistory>(fakeChats.Provider));

            mockSet.As<IQueryable<ChatHistory>>().Setup(m => m.Expression).Returns(fakeChats.Expression);
            mockSet.As<IQueryable<ChatHistory>>().Setup(m => m.ElementType).Returns(fakeChats.ElementType);
            mockSet.As<IQueryable<ChatHistory>>().Setup(m => m.GetEnumerator()).Returns(fakeChats.GetEnumerator());

            _chatRepoMock = new Mock<Repository<ChatHistory>>();
            _chatRepoMock.Setup(r => r.GetAll()).Returns(mockSet.Object);
            _db = new Mock<IUnitOfWork>();
            _db.Setup(db => db.ChatHistoryRepository).Returns(_chatRepoMock.Object);

            // Act
            _chatService = new ChatService(_db.Object);
            var chats = await _chatService.GetAllUserChatsAsync(1);
            var chats2 = await _chatService.GetAllUserChatsAsync(2);
            var chats3 = await _chatService.GetAllUserChatsAsync(3);
            var chats4 = await _chatService.GetAllUserChatsAsync(4);

            // Assert
            Assert.AreEqual(3, chats.Count());
            Assert.AreEqual(1, chats2.Count());
            Assert.AreEqual(1, chats3.Count());
            Assert.AreEqual(0, chats4.Count());
        }
    }
}
