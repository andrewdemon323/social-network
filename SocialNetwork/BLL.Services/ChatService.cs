﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.ChatDTOs;
using BLL.Interfaces.DTOs.MessageDTOs;
using BLL.Interfaces.DTOs.UserDTOs;
using BLL.Services.Utils;
using DatabaseProvider;
using DatabaseProvider.Entity;

namespace BLL.Services
{
    public class ChatService : IChatService
    {
        private const int CountMsgsPerMinute = 20;
        private readonly IUnitOfWork _db;
        private readonly MapperConfiguration _mapperConfig;

        public ChatService(IUnitOfWork db)
        {
            _db = db;

            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetails, UserDto>();
                cfg.CreateMap<UserDetails, ShortUserDto>();

                cfg.CreateMap<Message, MessageDto>()
                    .ForMember(dest => dest.Sender, opt => opt.MapFrom(
                        src => src.Sender.UserDetails));

                cfg.CreateMap<ChatHistory, ChatDto>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ChatId))
                    .ForMember(dest => dest.IsDialog, opt => opt.MapFrom(src => src.Chat.IsDialog))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(
                        src => ChatManager.BuildHistoryName(_db, src)))
                    .ForMember(dest => dest.Creator, opt => opt.MapFrom(src =>
                        src.Chat.Creator.UserDetails))
                    .ForMember(dest => dest.CreateTime, opt => opt.MapFrom(src => src.Chat.CreateTime))
                    .ForMember(dest => dest.LastMessage, opt => opt.MapFrom(
                        src =>
                            src.Messages.OrderByDescending(msg => msg.Time).FirstOrDefault()));
            });
        }

        public async Task<Temperature> GetTemperatureAsync(int id)
        {
            var chat = await _db.ChatRepository.GetAll().Where(ch => ch.Id == id).FirstOrDefaultAsync();
            var result = Temperature.Frozen;
            var now = DateTime.Now;
            if (chat.Messages.Count > 0)
            {
                var countMsgForLastMinute = chat.Messages.Count(m => m.Time.Minute >= now.Minute - 1);
                var lastMsgTime = chat.Messages.Max(m => m.Time);

                if ((now - lastMsgTime).Days <= 3)
                {
                    result = Temperature.Cold;
                }
                if ((now - lastMsgTime).Days < 1)
                {
                    result = Temperature.Warm;
                }
                if (countMsgForLastMinute >= CountMsgsPerMinute)
                {
                    result = Temperature.Hot;
                }
            }
            return result;
        }

        public async Task<IEnumerable<ChatDto>> GetAllUserChatsAsync(int userId)
        {
            var chats =
                await _db.ChatHistoryRepository.GetAll().Where(h => h.UserId == userId)
                    .OrderByDescending(h =>
                        h.Messages.Count > 0 ? h.Messages.Max(m => m.Time) : h.Chat.CreateTime)
                    
                    .ToListAsync();
            var mapper = _mapperConfig.CreateMapper();
            return mapper.Map<List<ChatHistory>, IEnumerable<ChatDto>>(chats);
        }

        public async Task<IEnumerable<ChatDto>> GetUserChatsPagedAsync(int userId, int take, int skip)
        {
            var chats =
                await _db.ChatHistoryRepository.GetAll().Where(h => h.UserId == userId)
                .OrderByDescending(h=>h.Messages.Max(m=>m.Time))
                .Skip(skip).Take(take).ToListAsync();
            var mapper = _mapperConfig.CreateMapper();
            return mapper.Map<List<ChatHistory>, IEnumerable<ChatDto>>(chats);
        }

        public async Task<IEnumerable<MessageDto>> GetMessagesAsync(int chatId, int userId)
        {
            var messages = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.ChatId == chatId && h.UserId == userId)
                .Select(h => h.Messages.OrderByDescending(m=>m.Time))
                .SingleOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<IEnumerable<Message>, IEnumerable<MessageDto>>(messages);
            return result;
        }

        public async Task<IEnumerable<MessageDto>> GetMessagesPagedAsync(int chatId, int userId, int skip,  int take)
        {
            var messages = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.ChatId == chatId && h.UserId == userId)
                .Select(h => h.Messages.OrderByDescending(m => m.Time)
                    .Skip(skip).Take(take))
                .SingleOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<IEnumerable<Message>, IEnumerable<MessageDto>>(messages);
            return result;
        }

        public async Task<ChatDto> GetByIdAsync(int chatId, int userId)
        {
            var chatHistory = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.ChatId == chatId
                         && h.UserId == userId)
                .SingleOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<ChatHistory, ChatDto>(chatHistory);
            return result;
        }

        public async Task<IEnumerable<ChatDto>> FindByNameOrMemberAsync(int userId, string criteria)
        {
            var histories = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.UserId == userId &&
                            (h.Chat.Name.StartsWith(criteria)
                             || h.Chat.Members.Any(m =>
                                 m.UserDetails.FirstName.StartsWith(criteria)
                                 || m.UserDetails.LastName.StartsWith(criteria))))
                .OrderByDescending(h => h.Messages.Max(m => m.Time))
                .ToListAsync();
            var mapper = _mapperConfig.CreateMapper();
            return mapper.Map<List<ChatHistory>, IEnumerable<ChatDto>>(histories);
        }

        public async Task<IEnumerable<ChatDto>> FindByNameOrMemberPagedAsync(int userId, string criteria, int skip,
            int take)
        {
            var histories = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.UserId == userId &&
                            (h.Chat.Name.StartsWith(criteria)
                             || h.Chat.Members.Any(m =>
                                 m.UserDetails.FirstName.StartsWith(criteria)
                                 || m.UserDetails.LastName.StartsWith(criteria))))
                .OrderByDescending(h => h.Messages.Max(m => m.Time))
                .Skip(skip).Take(take)
                .ToListAsync();
            var mapper = _mapperConfig.CreateMapper();
            return mapper.Map<List<ChatHistory>, IEnumerable<ChatDto>>(histories);
        }

        public async Task<IEnumerable<ShortUserDto>> GetMembersAsync(int chatId, int userId)
        {
            var isChatMember = await _db.ChatRepository.GetAll()
                .Where(ch => ch.Id == chatId)
                .Select(ch => ch.Members.Any(m => m.Id == userId)).SingleOrDefaultAsync();

            if (!isChatMember)
                throw new Exception("you can't get chat members because you are not member of this chat");

            var members = await _db.ChatRepository.GetAll()
                .Where(ch => ch.Id == chatId)
                .Select(ch => ch.Members.Select(m => m.UserDetails))
                .SingleOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<IEnumerable<UserDetails>, IEnumerable<ShortUserDto>>(members);
            return result;

        }

        public async Task<int> CreateChat(ChatDto chatDto, IEnumerable<int> membersIds)
        {
            List<AppUser> users = new List<AppUser>();
            var creator = _db.UserRepository.Get(chatDto.Creator.Id);
            users.Add(creator);
            foreach (var memberId in membersIds)
            {
                if(users.Any(u=>u.Id==memberId)) continue;
                var user = _db.UserRepository.Get(memberId);
                if (user == null) continue;
                users.Add(user);
            }

            var chat = new Chat()
            {
                Name = chatDto.Name,
                IsDialog = chatDto.IsDialog,
                CreatorId = creator.Id,
                CreateTime = DateTime.Now,
                Members = users
            };
            
            foreach (var user in users)
            {
                if(chat.ChatHistories.All(h => h.UserId != user.Id))
                {
                    var history = new ChatHistory() { ChatId = chat.Id, User = user, UserId = user.Id };
                    chat.ChatHistories.Add(history);
                }

            }
            
            _db.ChatRepository.Add(chat);
            await _db.SaveChangesAsync();
            return chat.Id;
        }

        public async Task AddUserToChat(int chatId, int userId, int currentUserId)
        {
            var chat = await _db.ChatRepository.GetAll()
                .Where(ch => ch.Id == chatId
                             && ch.Members.Any(m => m.Id == currentUserId))
                .FirstOrDefaultAsync();
            if (chat == null) throw new Exception("chat not found or you are not member of this chat");
            if (chat.Members.Any(m => m.Id == userId))
                throw new Exception("This user is already in chat");
            var user = await _db.UserRepository.GetAll().Where(u => u.Id == userId).SingleOrDefaultAsync();
            if (user == null)
                throw new Exception("User was not found");
            chat.Members.Add(user);
            var historyExists = await _db.ChatHistoryRepository.GetAll()
                .AnyAsync(h => h.ChatId == chatId
                               && h.UserId == userId);
            if (!historyExists)
            {
                var history = new ChatHistory() {ChatId = chat.Id, User = user, UserId = user.Id};
                _db.ChatHistoryRepository.Add(history);
                chat.ChatHistories.Add(history);
            }
            _db.ChatRepository.Update(chat);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteUserFromChat(int chatId, int userId, int currentUserId)
        {
            var chat = await _db.ChatRepository.GetAll()
                .SingleOrDefaultAsync(ch => ch.Id == chatId
                                            && ch.Members.Any(m => m.Id == userId));
            if (chat == null)
                throw new Exception("chat not found or user isn't member of this chat");
            var member = chat.Members.FirstOrDefault(m => m.Id == userId);
            if (currentUserId == chat.CreatorId || member?.Id == currentUserId)
            {
                chat.Members.Remove(member);
                _db.ChatRepository.Update(chat);
                await _db.SaveChangesAsync();
            }
            else
            {
                throw new Exception("You don't have permissions to delete user from this chat");
            }
        }

        public async Task DeleteChat(int chatId, int userId)
        {
            var chatHistory = await _db.ChatHistoryRepository.GetAll()
                .SingleOrDefaultAsync(h => h.ChatId == chatId
                                           && h.UserId == userId);
            if (chatHistory == null)
                throw new Exception("chat not found");
            chatHistory.Messages.Clear();
            chatHistory.Deleted = true;
            _db.ChatHistoryRepository.Update(chatHistory);
            await _db.SaveChangesAsync();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}