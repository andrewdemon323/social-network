﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.UserDTOs;
using DatabaseProvider;
using DatabaseProvider.Entity;

namespace BLL.Services
{
    public class FriendService : IFriendService
    {
        private readonly IUnitOfWork _db;
        private readonly MapperConfiguration _mapperConfig;

        public FriendService(IUnitOfWork db)
        {
            _db = db;
            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetails, UserDto>();
            });
        }

        public async Task<IEnumerable<UserDto>> GetAllFriendsAsync(int userId)
        {
            var friends = await _db.UserRepository
                .Find(u => u.Id == userId)
                .Select(u => u.Friends
                    .Select(f=>f.UserDetails))
                .SingleOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<IEnumerable<UserDetails>, IEnumerable<UserDto>>(friends);
            return result;
        }

        public async Task AddUserToFriendsAsync(int currentUserId, int userId)
        {
            var currentUser = await _db.UserRepository.GetAll()
                .Where(u => u.Id == currentUserId).SingleOrDefaultAsync();
            bool areFriends = currentUser.Friends.Any(f => f.Id == userId);
            if (areFriends)
                throw new Exception("You are already friends");
            var user = await _db.UserRepository.GetAll()
                .Where(u => u.Id == userId).SingleOrDefaultAsync();
            if(user==null)
                throw new Exception("User not found");
            currentUser.Friends.Add(user);
            _db.UserRepository.Update(currentUser);
            await _db.SaveChangesAsync();
        }

        public async Task RemoveUserFromFriendsAsync(int currentUserId, int userId)
        {
            var currentUser = await _db.UserRepository.GetAll()
                .Where(u => u.Id == currentUserId).SingleOrDefaultAsync();
            bool areFriends = currentUser.Friends.Any(f => f.Id == userId);
            if (!areFriends)
                throw new Exception("This user is not your friend");
            var user = await _db.UserRepository.GetAll()
                .Where(u => u.Id == userId).SingleOrDefaultAsync();
            if (user == null)
                throw new Exception("User not found");
            currentUser.Friends.Remove(user);
            _db.UserRepository.Update(currentUser);
            await _db.SaveChangesAsync();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}