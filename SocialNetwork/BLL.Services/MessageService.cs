﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs;
using BLL.Interfaces.DTOs.MessageDTOs;
using BLL.Interfaces.DTOs.UserDTOs;
using BLL.Services.Utils;
using DatabaseProvider;
using DatabaseProvider.Entity;

namespace BLL.Services
{
    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _db;
        private readonly MapperConfiguration _mapperConfig;

        public MessageService(IUnitOfWork db)
        {
            _db = db;
            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDetails, UserDto>();
                cfg.CreateMap<Message, MessageDto>()
                .ForMember(dest => dest.Sender, opt => opt.MapFrom(
                        src => src.Sender.UserDetails));
                cfg.CreateMap<SendMessageDto, Message>()
                    .ForMember(dest => dest.Time, opt => opt.UseValue(DateTime.Now));
            });
        }

        public IEnumerable<MessageDto> GetMessagesByChatId(int chatId)
        {
            ICollection<Message> messages = _db.ChatRepository.Get(chatId).Messages;
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<ICollection<Message>, IEnumerable<MessageDto>>(messages);
            return result;
        }

        public async Task<int> SendMessageToUser(SendMessageDto message)
        {
            var sender = _db.UserRepository.Get(message.SenderId);
            var mapper = _mapperConfig.CreateMapper();
            var msg = mapper.Map<SendMessageDto, Message>(message);
            var dialog = await _db.ChatRepository.Find(
                ch =>ch.IsDialog &&
                ch.Members.Any(u=>u.Id==msg.SenderId) &&
                ch.Members.Any(u=>u.Id==message.ReceiverId)
            ).SingleOrDefaultAsync();
            if (dialog == null)
            {
                using (var transaction = _db.BeginTransaction())
                {
                    try
                    {
                        var receiver = _db.UserRepository.Get(message.ReceiverId);
                        if (receiver == null)
                            throw new Exception("receiver not found");
                        if (!sender.Friends.Contains(receiver))
                        {
                            sender.Friends.Add(receiver);
                            _db.UserRepository.Update(sender);
                        }

                        Chat chat = new Chat()
                        {
                            Creator = sender,
                            CreatorId = sender.Id,
                            Members =
                            {
                                sender,
                                receiver
                            },
                            IsDialog = true
                        };

                        _db.ChatRepository.Add(chat);
                        _db.SaveChanges();
                        msg.ChatId = chat.Id;
                        
                        foreach (var member in chat.Members)
                        {
                            var history = new ChatHistory() { ChatId = chat.Id, User = member, UserId = member.Id };
                            _db.ChatHistoryRepository.Add(history);
                        }
                        ChatManager.SendMessage(_db, msg);
                        transaction.Commit();
                        return msg.Id;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw e;
                    }
                }
            }
            dialog.Messages.Add(msg);
            _db.ChatRepository.Update(dialog);
            ChatManager.SendMessage(_db, msg);
            _db.SaveChanges();
            return msg.Id;
        }

        public async Task<int> SendMessageToChat(SendMessageDto message)
        {
            var sender = await _db.UserManager.FindByIdAsync(message.SenderId);
            var chat = _db.ChatRepository.Get(message.ChatId);
            if (sender == null)
                throw new Exception("sender not found");
            if (chat == null)
                throw new Exception("chat not found");
            message.SenderId = sender.Id;
            var mapper = _mapperConfig.CreateMapper();
            var msg = mapper.Map<SendMessageDto, Message>(message);
            ChatManager.SendMessage(_db, msg);
            return msg.Id;
        }

        public async Task DeleteMessage(int messageId, int userId)
        {
            var history = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.UserId == userId
                            && h.Messages.Any(m => m.Id == messageId))
                .SingleOrDefaultAsync();
            if(history==null)
                throw new Exception("message not found");
            var message = history.Messages.SingleOrDefault(m => m.Id == messageId);
            history.Messages.Remove(message);
            _db.ChatHistoryRepository.Update(history);
            await _db.SaveChangesAsync();
        }

        public async Task<MessageDto> GetById(int id, int userId)
        {
            var msg = await _db.ChatHistoryRepository.GetAll()
                .Where(h => h.UserId == userId
                            && h.Messages.Any(m => m.Id == id))
                .Select(h => h.Messages.FirstOrDefault(m => m.Id == id)).SingleOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<Message, MessageDto>(msg);
            return result;
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}