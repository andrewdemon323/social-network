﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Interfaces;
using BLL.Interfaces.DTOs.UserDTOs;
using BLL.Services.Utils;
using DatabaseProvider;
using DatabaseProvider.Entity;
using Microsoft.AspNet.Identity;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _db;
        private readonly MapperConfiguration _mapperConfig;

        public UserService(IUnitOfWork db)
        {
            _db = db;
            _mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDto, UserDetails>();
                cfg.CreateMap<UserDetails, UserDto>()
                    .ForMember(dest => dest.ImgPath, opt =>
                        opt.MapFrom(src => src.ImagePath));
                cfg.CreateMap<AppUser, AccountDto>();
                cfg.CreateMap<AccountDto, AppUser>();
            });
        }

        public async Task<List<UserDto>> GetTheShortestPath(int fromUserId, int toUserId)
        {
            PathFinder finder = new PathFinder(_db);
            var path = await finder.Path(fromUserId, toUserId);
            var result = new List<UserDto>();
            var mapper = _mapperConfig.CreateMapper();
            foreach (var uId in path)
            {
                var user = await _db.UserRepository.GetAll().Where(u => u.Id == uId).Select(u=>u.UserDetails).FirstOrDefaultAsync();
                result.Add(mapper.Map<UserDetails, UserDto>(user));
            }
            return result;
        }

        public async Task<UserDto> FindByIdAsync(int userId)
        {
            var user = await _db.UserManager.FindByIdAsync(userId);
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<UserDetails, UserDto>(user.UserDetails);
            return result;
        }

        public async Task<int> GetUserIdByUserNameAsync(string userName)
        {
            var id =
                await _db.UserRepository.GetAll()
                    .Where(u => u.UserName.Equals(userName))
                    .Select(u => u.Id)
                    .SingleOrDefaultAsync();
            return id;
        }

        public async Task<AccountDto> FindAsync(string userName, string password)
        {
            var user = await _db.UserManager.FindAsync(userName, password);
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<AppUser, AccountDto>(user);
            return result;
        }

        public async Task<IdentityResult> CreateAsync(AccountDto account, UserDto userInfo)
        {
            var mapper = _mapperConfig.CreateMapper();
            var appUser = mapper.Map<AccountDto, AppUser>(account);
            appUser.Deleted = false;
            var result = await _db.UserManager.CreateAsync(appUser, account.Password);
            if (result.Errors != null && result.Errors.Any())
            {
                return result;
            }
            userInfo.RegDate = DateTime.Now;
            var details = mapper.Map<UserDto, UserDetails>(userInfo);
            details.Id = appUser.Id;
            _db.UserDetailsRepository.Add(details);
            await _db.SaveChangesAsync();
            return result;
        }

        public async Task<IEnumerable<UserDto>> GetAllUsersAsync(int take=0, int skip=0)
        {
            var users = _db.UserDetailsRepository.GetAll();
            if (take > 0)
            {
                users = users.OrderBy(u=>u.Id).Skip(skip).Take(take);
            }
            var filtered = await users.ToListAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<ICollection<UserDetails>, IEnumerable<UserDto>>(filtered);
            return result;
        }

        public async Task<IEnumerable<UserDto>> FindByCriteriaAsync(SearchCriteria criteria, int take=0, int skip=0)
        {
            var users = _db.UserDetailsRepository.GetAll();
            Filter filter = new Filter();
            users = filter.Filtrate(criteria, users);
            if (take > 0)
            {
                users = users.OrderBy(u=>u.Id).Skip(skip).Take(take);
            }
            var filtered = await users.ToListAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = mapper.Map<List<UserDetails>, IEnumerable<UserDto>>(filtered);
            return result;
        }

        public async Task<UserDto> GetDetailsByIdAsync(int userId)
        {
            var user = await _db.UserDetailsRepository.Find(u=>u.Id==userId).FirstOrDefaultAsync();
            var mapper = _mapperConfig.CreateMapper();
            var result = Mapper.Map<UserDetails, UserDto>(user);
            return result;
        }

        public async Task<IEnumerable<UserDto>> GetFriendsByUserIdAsync(int userId)
        {
            var result = new List<UserDto>();
            var mapper = _mapperConfig.CreateMapper();
            var user = await _db.UserRepository.Find(u => u.Id == userId).FirstOrDefaultAsync();
            foreach (var f in user.Friends)
            {
                var friend = mapper.Map<UserDetails, UserDto>(f.UserDetails);
                result.Add(friend);
            }
            return result;
        }

        public async Task<string> GetImagePathAsync(int userId)
        {
            var result = await _db.UserRepository.GetAll()
                    .Where(u => u.Id == userId)
                    .Select(u => u.UserDetails.ImagePath)
                    .FirstOrDefaultAsync();
            return result;
        }

        public void UpdateImagePath(int userId, string path)
        {
            var userDetails = _db.UserDetailsRepository.Get(userId);
            userDetails.ImagePath = path;
            userDetails.LastModif = DateTime.Now;
            _db.UserDetailsRepository.Update(userDetails);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}