﻿using System.Data.Entity;
using System.Linq;
using System.Text;
using DatabaseProvider;
using DatabaseProvider.Entity;

namespace BLL.Services.Utils
{
    public class ChatManager
    {
        public static string BuildHistoryName(IUnitOfWork db, ChatHistory history)
        {
            var chat = db.ChatRepository.GetAll().SingleOrDefault(ch => ch.Id == history.ChatId);
            if (chat != null)
                if (chat.Name != null)
                {
                    return chat.Name;
                }
                else
                {
                    var nameBuilder = new StringBuilder();
                    foreach (var member in chat.Members)
                    {
                        if (member.Id != history.UserId)
                        {
                            nameBuilder.Append($"{member.UserDetails.FirstName} {member.UserDetails.LastName}, ");
                        }
                    }
                    nameBuilder.Remove(nameBuilder.Length - 2, 2);
                    return nameBuilder.ToString();
                }
            return null;
        }

        public static void SendMessage(IUnitOfWork db, Message msg)
        {
            db.MessageRepository.Add(msg);
            var chat = db.ChatRepository.GetAll().SingleOrDefault(ch => ch.Id == msg.ChatId);
            if (chat != null)
                foreach (var member in chat.Members)
                {
                    var chatHistory = member.ChatHistories.SingleOrDefault(h => h.ChatId == chat.Id);
                    if (chatHistory != null)
                    {
                        chatHistory.Deleted = false;
                        chatHistory.Messages.Add(msg);
                    }
                }
            db.SaveChanges();
        }
    }
}