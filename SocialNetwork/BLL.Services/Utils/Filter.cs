﻿using System;
using System.Linq;
using BLL.Interfaces;
using DatabaseProvider.Entity;

namespace BLL.Services.Utils
{
    public class Filter
    {
        public IQueryable<UserDetails> Filtrate(SearchCriteria criteria, IQueryable<UserDetails> users)
        {
            
            if (criteria.Name?.Length > 0)
            {
                var parts = SplitNameIfPossible(criteria.Name);
                if (parts.Length == 1)
                {
                    users = users.Where(u =>u.FirstName.StartsWith(criteria.Name)
                             || u.LastName.StartsWith(criteria.Name));
                }
                else
                {
                    var part1 = parts[0];
                    var part2 = parts[1];
                    users = users.Where(u =>(u.FirstName.StartsWith(part1)
                             && u.LastName.StartsWith(part2))
                             ||(u.FirstName.StartsWith(part2)
                             && u.LastName.StartsWith(part1)));
                }
            }
            if (criteria.Gender != null)
            {
                users = users.Where(u => u.Gender == criteria.Gender.Value);
            }
            var now = DateTime.Now.Year;
            if (criteria.MaxAge != null)
            {
                users = users.Where(u => u.DateOfBirth.HasValue 
                && now - u.DateOfBirth.Value.Year <= criteria.MaxAge.Value);
            }
            if (criteria.MinAge != null)
            {
                users = users.Where(u => u.DateOfBirth.HasValue 
                && now - u.DateOfBirth.Value.Year >= criteria.MinAge.Value);
            }
            return users;
        }

        private string[] SplitNameIfPossible(string name)
        {
            return name.Trim(' ').Split(' ');
        }
    }
}