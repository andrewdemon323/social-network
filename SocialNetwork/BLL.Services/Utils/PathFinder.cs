﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.XpressionMapper;
using BLL.Interfaces;
using DatabaseProvider;

namespace BLL.Services.Utils
{
    public class PathFinder
    {
        private Dictionary<int, int> _id = new Dictionary<int, int>();
        private Dictionary<int, int> _path = new Dictionary<int, int>();
        private IUnitOfWork _db;

        public PathFinder(IUnitOfWork db)
        {
            
            _db = db;
        }

        private async Task Go(int from, int to, int deep)
        {
            if(from==to) return;
            var friends = await GetFriendsIds(from);
            if (friends.Count == 0) return;
            foreach (var f in friends)
            {
                if (!_id.ContainsKey(f) || deep < _id[f])
                {
                    _id[f] = deep;
                    _path[f] = from;
                    await Go(f, to, deep + 1);
                }
            }
        }

        public async Task<List<int>> Path(int from, int to)
        {
            var result = new List<int>();
            _id[from] = -1;
            await Go(from, to, 0);
            if (!_path.ContainsKey(to)) return result;
            var i = to;
            while (i != from)
            {
                result.Insert(0, i);
                i = _path[i];
            }
            return result;
        }

        private async Task<List<int>> GetFriendsIds(int p)
        {
            var res =
                await _db.UserRepository.GetAll().Where(u => u.Id == p)
                    .Select(u => u.Friends
                        .Select(f => f.Id)).FirstOrDefaultAsync();
            return res.ToList();
        }


    }
}