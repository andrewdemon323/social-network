﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace ConsoleApp.InputParser.Command
{
    public class AddCommand<T> : ICommand where T : class, IEntity
    {
        private IRepository<T> repository;
        private T t;

        public AddCommand(IRepository<T> repository, T t)
        {
            this.repository = repository;
            this.t = t;
        }

        public void Execute()
        {
            
            repository.Add(t);
            //repository.Dispose();

            Console.WriteLine("Add executed\n");
        }
    }
}
