﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace ConsoleApp.InputParser.Command
{
    public class DeleteCommand<T> : ICommand where T : class, IEntity
    {
        private IRepository<T> repository;
        private int id;

        public DeleteCommand(IRepository<T> repository, int id)
        {
            this.repository = repository;
            this.id = id;
        }

        public void Execute()
        {
            T entity = repository.Get(id);
            repository.Delete(entity);
            //repository.Dispose();

            Console.WriteLine("Delete executed\n");
        }
    }
}
