﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace ConsoleApp.InputParser.Command
{
    class GetAllCommand<T> : ICommand where T : class, IEntity
    {
        private IRepository<T> repository;

        public GetAllCommand(IRepository<T> repository)
        {
            this.repository = repository;
        }

        public void Execute()
        {
            IEnumerable<T> items = repository.GetAll();
            foreach (T item in items)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine();
        }
    }
}
