﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace ConsoleApp.InputParser.Command
{
    public class GetCommand<T> : ICommand where T :class, IEntity
    {
        private IRepository<T> repository;
        private int id;
        public T Object { get; private set; }

        public GetCommand (IRepository<T> repository, int id)
        {
            this.repository = repository;
            this.id = id;
        }

        public void Execute()
        {
            Object = repository.Get(id);
            Console.WriteLine(Object.ToString() + "\n");
           // repository.Dispose();
        }
    }
}
