﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;

namespace ConsoleApp.InputParser.Command
{
    class GetStorageCommand : ICommand
    {
        public void Execute()
        {
            String storage = Properties.Settings.Default.Storage;
            Console.WriteLine("Current storage : " + storage + "\n");
        }
    }
}
