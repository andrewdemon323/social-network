﻿using System;
using ConsoleApp.InputParser.Interface;

namespace ConsoleApp.InputParser.Command
{
    internal class SetStorageCommand : ICommand
    {
        private string storage;

        public SetStorageCommand(string storage)
        {
            this.storage = storage;
        }

        public void Execute()
        {
            Properties.Settings.Default.Storage = storage;

            Console.WriteLine("Set storage executed\n");
        }
    }
}