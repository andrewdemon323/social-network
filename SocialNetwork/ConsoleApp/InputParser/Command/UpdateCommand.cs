﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace ConsoleApp.InputParser.Command
{
    public class UpdateCommand<T> : ICommand where T : class, IEntity
    {
        private IRepository<T> repository;
        private T t;

        public UpdateCommand (IRepository<T> repository, T t)
        {
            this.repository = repository;
            this.t = t;
        }

        public void Execute()
        {
            repository.Update(t);
            //repository.Dispose();

            Console.WriteLine("Update executed\n");
        }
    }
}
