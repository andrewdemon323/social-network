﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace ConsoleApp.InputParser.Expression
{
    public class ActionExpression : IExpression
    {

        String action;

        public ActionExpression(String action)
        {
            this.action = action;
        }

        public void Interpret(IInterpreterContext context)
        {
            var actions = context.GetActions();
            Action actionDelegate;
            actions.TryGetValue(action, out actionDelegate);
            if (actionDelegate != null)
            {
                actionDelegate();
            } else
            {
                throw new ArgumentNullException("No such action: " + action);
            }
                
        }
    }
}
