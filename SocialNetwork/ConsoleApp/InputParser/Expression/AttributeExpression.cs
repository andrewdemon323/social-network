﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;

namespace ConsoleApp.InputParser.Expression
{
    public class AttributeExpression : IExpression
    {
        private String property;

        public AttributeExpression(String property)
        {
            this.property = property;
        }

        public void Interpret(IInterpreterContext context)
        {
            String[] pair = property.Split(':');
            if (pair.Length == 2)
            {
                String key = pair[0];
                String value = pair[1];
                context.GetProperties().Add(key, value);
            } else
            {
                throw new ArgumentNullException();
            }
        }
    }
}
