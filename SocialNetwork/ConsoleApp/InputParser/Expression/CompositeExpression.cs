﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Expression;
using ConsoleApp.InputParser.Interface;

namespace ConsoleApp.InputParser.Expression
{
    public class CompositeExpression : IExpression
    {
        private ActionExpression action;
        private EntityExpression entity;
        private List<AttributeExpression> attributes;

        public CompositeExpression(ActionExpression action, EntityExpression entity, List<AttributeExpression> attributes)
        {
            this.action = action;
            this.entity = entity;
            this.attributes = attributes;
        }

        public void Interpret(IInterpreterContext context)
        {
            foreach(var attribute in attributes)
            {
                attribute.Interpret(context);
            }
            entity.Interpret(context);
            action.Interpret(context);

            ICommand command = context.GetCommand();
            if (command != null)
            {
                command.Execute();
            }
        }
    }
}
