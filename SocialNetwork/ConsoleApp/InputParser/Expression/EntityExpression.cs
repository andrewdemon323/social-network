﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Interface;

namespace ConsoleApp.InputParser.Expression
{
    public class EntityExpression : IExpression
    {
        private String subject;

        public EntityExpression(String subject)
        {
            this.subject = subject;
        }

        public void Interpret(IInterpreterContext context)
        {
            var entities = context.GetEntitites();
            var settings = context.GetSettings();
            Dictionary<String, Action> actions = null;

            if (entities.ContainsKey(subject))
            {
                entities.TryGetValue(subject, out actions);
                context.SetActions(actions);
                return;
            }

            if (settings.ContainsKey(subject))
            {
                settings.TryGetValue(subject, out actions);
                context.SetActions(actions);
                return;
            }

            throw new ArgumentNullException("No such entity: " + subject);
        }
    }
}
