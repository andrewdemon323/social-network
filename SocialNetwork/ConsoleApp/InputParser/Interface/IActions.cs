﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.InputParser.Interface
{
    interface IActions
    {
       Dictionary<String, Dictionary<String, Action>> GetActions(string entity);
    }
}
