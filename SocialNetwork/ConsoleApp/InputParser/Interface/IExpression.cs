﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.InputParser.Interface
{
    public interface IExpression
    {
        void Interpret(IInterpreterContext context);
    }
}
