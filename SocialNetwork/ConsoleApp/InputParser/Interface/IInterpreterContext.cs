﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.InputParser.Interface
{
    public interface IInterpreterContext
    {
        Dictionary<String, String> GetProperties();
        Dictionary<String, Dictionary<String, Action>> GetEntitites();
        Dictionary<String, Dictionary<String, Action>> GetSettings();
        void SetActions(Dictionary<String, Action> actions);
        Dictionary<String, Action> GetActions();
        ICommand GetCommand();
    }
}
