﻿using System;
using ConsoleApp.InputParser.Expression;

namespace ConsoleApp.InputParser.Interface
{
    interface IParser
    {
        CompositeExpression Parse(String input);
    }
}
