﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser.Command;
using ConsoleApp.InputParser.Interface;
using DatabaseProvider;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;
using JSONDatabase;

namespace ConsoleApp.InputParser
{
    class InterpreterContext : IInterpreterContext
    {
        const String USER = "user";
        const String STORAGE = "storage";
        const String PROGRAM = "program";
        const String ADD = "add";
        const String UPDATE = "update";
        const String REMOVE = "remove";
        const String GET = "get";
        const String GET_ALL = "get-all";
        const String SET = "set";
        const String EXIT = "exit";
        const String FIRST_NAME = "-fn";
        const String LAST_NAME = "-ln";
        const String ID = "-id";
        const String STORAGE_TYPE = "-type";
        const String HELP = "help";
        private ICommand Command { get; set; }
        public Dictionary<String, Dictionary<String, Action>> Entities { get; private set; }
        public Dictionary<String, Dictionary<String, Action>> Settings { get; private set; }
        public Dictionary<String, Action> SelectedActions { get; set; }
        private Dictionary<String, String> properties;
        public InterpreterContext(Action exitAction)
        {
            Entities = new Dictionary<String, Dictionary<String, Action>>();
            Settings = new Dictionary<string, Dictionary<string, Action>>();
            properties = new Dictionary<string, string>();
            Dictionary<String, Action> userActions = new Dictionary<string, Action>();
            Dictionary<String, Action> storageActions = new Dictionary<string, Action>();
            Dictionary<String, Action> programActions = new Dictionary<string, Action>();
            Entities.Add(USER, userActions);
            Settings.Add(STORAGE, storageActions);
            Settings.Add(PROGRAM, programActions);
            Action addUser = () =>
            {
                IRepository<UserDetails> repository;
                repository = GetRepository<UserDetails>();
                String firstName;
                String lastName;
                properties.TryGetValue(FIRST_NAME, out firstName);
                properties.TryGetValue(LAST_NAME, out lastName);
                if (firstName != null && lastName != null)
                {
                    UserDetails userDetails = new UserDetails();
                    userDetails.FirstName = firstName;
                    userDetails.LastName = lastName;
                    userDetails.Id = Properties.Settings.Default.NextUserId;
                    Properties.Settings.Default.NextUserId += 1;
                    Command = new AddCommand<UserDetails>(repository, userDetails);
                }
                else
                {
                    throw new ArgumentNullException(String.Format("valid command: add user {0}:value {1}:value", FIRST_NAME, LAST_NAME));
                }
            };
            Action updateUser = () =>
            {
                IRepository<UserDetails> repository;
                repository = GetRepository<UserDetails>();
                String firstName;
                String lastName;
                String id;
                properties.TryGetValue(FIRST_NAME, out firstName);
                properties.TryGetValue(LAST_NAME, out lastName);
                properties.TryGetValue(ID, out id);
                if (firstName != null && lastName != null && id != null)
                {
                    UserDetails userDetails = new UserDetails();
                    userDetails.FirstName = firstName;
                    userDetails.LastName = lastName;
                    userDetails.Id = int.Parse(id);
                    Command = new UpdateCommand<UserDetails>(repository, userDetails);
                }
                else
                {
                    throw new ArgumentNullException("valid command update user -id:id -fn:value -ln:value");
                }
            };
            Action removeUser = () =>
            {
                IRepository<UserDetails> repository;
                repository = GetRepository<UserDetails>();
                String id;
                properties.TryGetValue(ID, out id);
                if (id != null)
                {
                    Command = new DeleteCommand<UserDetails>(repository, int.Parse(id));
                }
                else
                {
                    throw new ArgumentNullException("valid command remove user -id:id");
                }
            };
            Action getUser = () =>
            {
                IRepository<UserDetails> repository;
                repository = GetRepository<UserDetails>();
                String id;
                properties.TryGetValue(ID, out id);
                if (id != null)
                {
                    Command = new GetCommand<UserDetails>(repository, int.Parse(id));
                }
                else
                {
                    throw new ArgumentNullException("valid command get user -id:id");
                }
            };
            Action getAllUsers = () =>
            {
                IRepository<UserDetails> repository;
                repository = GetRepository<UserDetails>();
                Command = new GetAllCommand<UserDetails>(repository);
            };
            Action setStorage = () =>
            {
                String storage;
                properties.TryGetValue(STORAGE_TYPE, out storage);
				if(storage != null && (storage.Equals("sql") || storage.Equals("json")))
                {
                    Command = new SetStorageCommand(storage);
                }
                else
                {
                    throw new ArgumentNullException("valid command set storage -type:sql|json");
                }

            };
            Action getStorage = () =>
            {
                Command = new GetStorageCommand();
            };

            Action helpToUser = () =>
            {
                Console.WriteLine("--'add user -fn:fn -ln:ln' to add new user \n" +
                                  "--'get user -id:id' to get specific user \n" +
                                  "--'get-all user' to get all users\n" +
                                  "--'remove user -id:id' to remove specific user \n" +
                                  "--'update user -id:id -fn:new_fn -ln:new_ln' to update already existed user \n" +
                                  "--'get storage' to get current storage\n" +
                                  "--'set storage -type:sql|json' to set storage type\n" +
                                  "--'exit' to exit the application\n");
            };
            Action exit = () => {
                exitAction();
            };
            userActions.Add(ADD, addUser);
            userActions.Add(GET, getUser);
            userActions.Add(GET_ALL, getAllUsers);
            userActions.Add(REMOVE, removeUser);
            userActions.Add(UPDATE, updateUser);
            storageActions.Add(SET, setStorage);
            storageActions.Add(GET, getStorage);
            programActions.Add(HELP, helpToUser);
            programActions.Add(EXIT, exit);
        }
        private IRepository<T> GetRepository<T>() where T : class, IEntity
        {
            String repoType = Properties.Settings.Default.Storage;
            IRepository<T> repository = null;
            RepositoryFactory<T> repositoryFactory;
            switch (repoType)
            {
                case "json":
                    {
                        repositoryFactory = new JsonRepositoryFactory<T>();
                        repository = repositoryFactory.GetRepository();
                        break;
                    }
                case "sql":
                    {
                        throw new NotImplementedException("no such repository type");
                        //todo:
                        //repositoryFactory = new DbRepositoryFactory<T>();
                        //repository = repositoryFactory.GetRepository();
                        //break;
                    }
                default:
                    {
                        break;
                    }
            }
            return repository;
        }
        public Dictionary<string, string> GetProperties()
        {
            return properties;
        }
        public ICommand GetCommand()
        {
            return Command;
        }
        public Dictionary<string, Dictionary<string, Action>> GetEntitites()
        {
            return Entities;
        }
        public Dictionary<string, Dictionary<string, Action>> GetSettings()
        {
            return Settings;
        }
        public void SetActions(Dictionary<string, Action> actions)
        {
            SelectedActions = actions;
        }
        public Dictionary<string, Action> GetActions()
        {
            return SelectedActions;
        }
    }
}
