﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser;
using ConsoleApp.InputParser.Expression;
using ConsoleApp.InputParser.Interface;

namespace ConsoleApp.InputParser
{
    class Parser : IParser
    {
        public CompositeExpression Parse(string input)
        {
            List<String> inputStrings = new List<String>(input.Split(' '));
            string str = inputStrings.First(); 

            var actionExpression = new ActionExpression(str);
            inputStrings.RemoveAt(0);

            if (inputStrings.Count != 0)
            {
                str = inputStrings.First();
                inputStrings.RemoveAt(0);
            } else
            {
                str = "program";
            }

            var entityExpression = new EntityExpression(str);


            List<AttributeExpression> attributes = new List<AttributeExpression>();
            foreach(String attribute in inputStrings)
            {
                var attributeExpression = new AttributeExpression(attribute);
                attributes.Add(attributeExpression);
            }

            var compositeExpresion = new CompositeExpression(actionExpression, entityExpression, attributes);

            return compositeExpresion;
        }

        public bool Execute()
        {
            throw new NotImplementedException();
        }
    }
}
