﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.InputParser;

namespace ConsoleApp
{
    class Program
    {
        static bool isRunning;
        public static void Main(string[] args)
        {
            
            isRunning = true;

            while (isRunning)
            {
                string input = Console.ReadLine();
                Parser p = new Parser();
                var expression = p.Parse(input);
                var context = new InterpreterContext(StopProgramExecution);
                try
                {
                    expression.Interpret(context);
                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (NotImplementedException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                
            }
        }

        static void StopProgramExecution()
        {
            isRunning = false;
        }
    }
}
