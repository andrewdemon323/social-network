﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DatabaseProvider.Entity
{
    public class AppUser : IdentityUser<int, Login, UserRole, Claim>, IEntity
    {
        public bool Deleted { get; set; }

        public virtual UserDetails UserDetails { get; set; }

        public virtual ICollection<AppUser> Friends { get; set; }

        //public virtual ICollection<Chat> Chats { get; set; }

        //public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<ChatHistory> ChatHistories { get; set; }

        public AppUser()
        {
            ChatHistories = new List<ChatHistory>();
            //Chats = new List<Chat>();
            //Messages = new List<Message>();
        }
    }
}