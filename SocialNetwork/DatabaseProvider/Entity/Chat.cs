﻿using System;
using System.Collections.Generic;

namespace DatabaseProvider.Entity
{
    public class Chat : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public int CreatorId { get; set; }

        public bool IsDialog { get; set; }

        public DateTime CreateTime { get; set; }

        public AppUser Creator { get; set; }

        public virtual ICollection<AppUser> Members { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<ChatHistory> ChatHistories { get; set; }

        public Chat()
        {
            IsDialog = false;
            Members = new List<AppUser>();
            Messages = new List<Message>();
            ChatHistories = new List<ChatHistory>();
        }
    }
}