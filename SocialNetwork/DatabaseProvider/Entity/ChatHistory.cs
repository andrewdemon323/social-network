﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseProvider.Entity
{
    public class ChatHistory : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int ChatId { get; set; }

        public bool Deleted { get; set; }
        
        public virtual AppUser User { get; set; }

        public virtual Chat Chat { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public ChatHistory()
        {
            Messages = new List<Message>();
        }
    }
}
