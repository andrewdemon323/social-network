﻿namespace DatabaseProvider.Entity
{
    public enum Gender
    {
        Male,
        Female,
        Unknown
    }
}