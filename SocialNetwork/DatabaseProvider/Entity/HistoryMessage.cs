﻿namespace DatabaseProvider.Entity
{
    public class HistoryMessage : IEntity
    {
        public int Id { get; set; }
        public int ChatHistoryId { get; set; }
        public int MessageId { get; set; }
        public bool Read { get; set; }
    }
}