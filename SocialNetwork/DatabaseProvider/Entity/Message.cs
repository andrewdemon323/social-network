﻿using System;
using System.Collections.Generic;

namespace DatabaseProvider.Entity
{
    public class Message : IEntity
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int SenderId { get; set; }

        public int ChatId { get; set; }

        public DateTime Time { get; set; }

        public virtual AppUser Sender { get; set; }
    }
}
