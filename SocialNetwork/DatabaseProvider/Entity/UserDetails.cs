﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseProvider.Entity
{
    public class UserDetails : IEntity
    {
        [Key]
        [ForeignKey("User")]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? RegDate { get; set; }

        public string ImagePath { get; set; }

        public DateTime? LastModif { get; set; }

        public virtual AppUser User { get; set; }

        public override string ToString()
        {
            return String.Format("id:{0}, {1}, {2}", Id, FirstName, LastName);
        }

    }
}