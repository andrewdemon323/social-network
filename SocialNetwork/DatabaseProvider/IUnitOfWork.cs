﻿using System;
using System.Data.Entity;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DatabaseProvider.Entity;
using DatabaseProvider.Identity;
using DatabaseProvider.Repository;

namespace DatabaseProvider
{
    public interface IUnitOfWork : IDisposable
    {
        IUserManager<AppUser, int> UserManager { get; }

        IRoleManager<Role, int> RoleManager { get; }
        
        IRepository<AppUser> UserRepository { get; }

        IRepository<UserDetails> UserDetailsRepository { get; }

        IRepository<Message> MessageRepository { get; }

        IRepository<Chat> ChatRepository { get; }

        IRepository<Role> RoleRepository { get; }

        IRepository<ChatHistory> ChatHistoryRepository { get; }

        //IRepository<HistoryMessage> HistoryMessagesRepository { get; }

        DbContextTransaction BeginTransaction();

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}