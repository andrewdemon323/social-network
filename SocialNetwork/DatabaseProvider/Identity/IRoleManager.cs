﻿using System;
using Microsoft.AspNet.Identity;

namespace DatabaseProvider.Identity
{
    public interface IRoleManager<TRole, TKey> : IDisposable where TRole : class, IRole<TKey> where TKey : IEquatable<TKey>
    {
        
    }
}