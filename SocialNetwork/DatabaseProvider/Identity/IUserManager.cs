﻿using System;
using System.Threading.Tasks;
using DatabaseProvider.Entity;
using Microsoft.AspNet.Identity;

namespace DatabaseProvider.Identity
{
    public interface IUserManager<TUser, TKey> : IDisposable where TUser : class, IUser<TKey> where TKey : IEquatable<TKey>
    {
        Task<AppUser> FindByIdAsync(int userId);

        Task<AppUser> FindAsync(string userName, string password);

        Task<AppUser> FindByNameAsync(string userName);

        Task<IdentityResult> CreateAsync(AppUser user, string password);
    }
}