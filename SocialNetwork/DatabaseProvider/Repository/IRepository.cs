﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DatabaseProvider.Entity;

namespace DatabaseProvider.Repository
{
    public interface IRepository<T> where T : class, IEntity
    {
        void Add(T entity);
        T Get(int id);
        IQueryable<T> GetAll();
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        void Update(T entity);
        void Delete(T entity);
    }
}