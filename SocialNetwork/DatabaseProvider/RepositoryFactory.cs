﻿using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace DatabaseProvider
{
    public abstract class RepositoryFactory<T> where T : class, IEntity
    {
        public abstract IRepository<T> GetRepository();
    }
}