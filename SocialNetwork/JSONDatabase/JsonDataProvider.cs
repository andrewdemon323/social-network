﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;
using Newtonsoft.Json;

namespace JSONDatabase
{
    public class JsonDataProvider<T> : IRepository<T> where T : class, IEntity
    {
        public void Dispose()
        {
        }

        public void Add(T entity)
        {
            String path = Path.Combine(Directory.GetCurrentDirectory(), "Users.json");
            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
            var data = JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(path));
            if (data == null || data.Count==0)
            {
                data = new List<T>();
                entity.Id = 1;
            }
            else
            {
                entity.Id = data[data.Count-1].Id+1;
            }
            data.Add(entity);
            var usr = JsonConvert.SerializeObject(data);
            using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), @"Users.json")))
            {
                sw.WriteLine(usr);
            }
        }

        public T Get(int id)
        {
            var result = default(T);
            var data = JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "Users.json")));
            if (data == null)
            {
                Console.WriteLine("You have no users in your json. Create one first. ");
                //todo throw some exception maybe
            }
            else
            {
                result = data.Single(s => s.Id == id);
            }
            return result;
        }

        public IQueryable<T> GetAll()
        {
            var data = JsonConvert.DeserializeObject<IQueryable<T>>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "Users.json")));
            if (data == null)
            {
                Console.WriteLine("You have no users in your json. Create one first. ");
                //todo throw some exception maybe
            }
            return data;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            var data = JsonConvert.DeserializeObject<IQueryable<T>>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "Users.json")));
            
            if (data == null)
            {
                Console.WriteLine("You have no users in your json. Create one first. ");
                //todo throw some exception maybe
            }
            return data.Where(predicate);
        }

        public void Update(T entity)
        {
            var data = JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "Users.json")));
            if (data == null)
            {
                Console.WriteLine("You have no users in your json. Create one first. ");
                //todo throw some exception maybe
            }
            else
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].Id == entity.Id)
                    {
                        data[i] = entity;
                        break;
                    }
                }
                var usr = JsonConvert.SerializeObject(data);
                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), @"Users.json")))
                {
                    sw.WriteLine(usr);
                }
            }
        }

        public void Delete(T entity)
        {
            var data = JsonConvert.DeserializeObject<List<UserDetails>>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "Users.json")));
            if (data == null)
            {
                Console.WriteLine("You have no users in your json. Create one first. ");
                //todo throw some exception maybe
            }
            else
            {
                UserDetails result = data.Single(s => s.Id == entity.Id);
                data.Remove(result);

                var usr = JsonConvert.SerializeObject(data);
                using (StreamWriter sw = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), @"Users.json")))
                {
                    sw.WriteLine(usr);
                }
            }
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}