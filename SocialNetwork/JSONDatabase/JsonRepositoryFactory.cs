﻿using DatabaseProvider;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace JSONDatabase
{
    public class JsonRepositoryFactory<T> : RepositoryFactory<T> where T : class, IEntity
    {
        public override IRepository<T> GetRepository()
        {
            return new JsonDataProvider<T>();
        }
    }
}