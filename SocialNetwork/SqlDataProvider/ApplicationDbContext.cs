﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using DatabaseProvider;
using DatabaseProvider.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SqlDataProvider
{
    public class ApplicationDbContext : IdentityDbContext<AppUser, Role, int, Login, UserRole, Claim>
    {
        public ApplicationDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public ApplicationDbContext()
            : base("SocialNetwork")
        {
        }

        public IDbSet<UserDetails> UserDetailses { get; set; }

        public IDbSet<Message> Messages { get; set; }

        public IDbSet<Chat> Chats { get; set; }

        public IDbSet<ChatHistory> ChatHistories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<AppUser>().ToTable("Users")
               .HasMany(u => u.Friends)
               .WithMany()
               .Map(u =>
               {
                   u.ToTable("Friendships");
                   u.MapLeftKey("UserId");
                   u.MapRightKey("FriendId");
               });

            modelBuilder.Entity<AppUser>()
                .HasMany(u => u.ChatHistories)
                .WithRequired().WillCascadeOnDelete(true);

            modelBuilder.Entity<Role>().ToTable("Roles");

            modelBuilder.Entity<Claim>().ToTable("Claims");

            modelBuilder.Entity<Login>().ToTable("Logins");

            modelBuilder.Entity<UserRole>().ToTable("UserRoles");

            modelBuilder.Entity<Chat>().ToTable("Chats")
                .HasMany(ch => ch.Messages)
                .WithRequired();

            modelBuilder.Entity<Chat>()
                .HasMany(ch => ch.ChatHistories)
                .WithRequired(h=>h.Chat).WillCascadeOnDelete(false);

            modelBuilder.Entity<Chat>()
                .HasRequired(ch=>ch.Creator)
                .WithMany().WillCascadeOnDelete(false);

            modelBuilder.Entity<Chat>()
                .HasMany(ch => ch.Members)
                .WithMany()
                .Map(ch =>
                {
                    ch.ToTable("ChatMembers");
                    ch.MapLeftKey("ChatId");
                    ch.MapRightKey("MemberId");
                });

            modelBuilder.Entity<ChatHistory>().ToTable("ChatHistories")
                .HasMany(h => h.Messages)
                .WithMany()
                .Map(h =>
                {
                    h.ToTable("HistoriesMessages");
                    h.MapLeftKey("ChatHistoryId");
                    h.MapRightKey("MessageId");
                });

            modelBuilder.Entity<ChatHistory>()
                .HasRequired(h => h.User).WithMany(u => u.ChatHistories);

            modelBuilder.Entity<ChatHistory>()
                .HasRequired(h=>h.Chat)
                .WithMany(ch=>ch.ChatHistories).WillCascadeOnDelete(false);
        }
    }
}