﻿using DatabaseProvider.Entity;
using DatabaseProvider.Identity;
using Microsoft.AspNet.Identity;

namespace SqlDataProvider.Identity
{
    public class ApplicationRoleManager : RoleManager<Role, int>, IRoleManager<Role, int>
    {
        public ApplicationRoleManager(IRoleStore<Role, int> store) : base(store)
        {
        }
    }
}