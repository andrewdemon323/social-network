﻿using DatabaseProvider.Entity;
using DatabaseProvider.Identity;
using Microsoft.AspNet.Identity;

namespace SqlDataProvider.Identity
{
    public class ApplicationUserManager : UserManager<AppUser, int>, IUserManager<AppUser, int>
    {
        public ApplicationUserManager(IUserStore<AppUser, int> store) : base(store)
        {
        }
    }
}