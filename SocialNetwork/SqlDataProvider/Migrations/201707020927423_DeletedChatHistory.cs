namespace SqlDataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedChatHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChatHistories", "Deleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChatHistories", "Deleted");
        }
    }
}
