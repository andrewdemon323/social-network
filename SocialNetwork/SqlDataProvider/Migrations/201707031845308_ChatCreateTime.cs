namespace SqlDataProvider.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChatCreateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chats", "CreateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chats", "CreateTime");
        }
    }
}
