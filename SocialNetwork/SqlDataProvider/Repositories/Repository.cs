﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DatabaseProvider;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;

namespace SqlDataProvider.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly ApplicationDbContext _context;
   
        private DbSet<TEntity> _set;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Repository()
        {
            _context = new ApplicationDbContext();
        }

        private IDbSet<TEntity> Set()
        {
            return _set ?? (_set = _context.Set<TEntity>());
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return Set();
        }

        public virtual void Add(TEntity entity)
        {
            Set().Add(entity);
        }

        public virtual TEntity Get(int id)
        {
            return Set().Find(id);
        }

        public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Set().Where(predicate);
        }

        public virtual void Update(TEntity entity)
        {
            var entry = _context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                Set().Attach(entity);
                entry = _context.Entry(entity);
            }
            entry.State = EntityState.Modified;
        }

        public virtual void Delete(TEntity entity)
        {
            Set().Remove(entity);
        }
    }
}