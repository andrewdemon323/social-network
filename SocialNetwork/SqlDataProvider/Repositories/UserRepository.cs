﻿using System.Threading.Tasks;
using DatabaseProvider.Entity;
using DatabaseProvider.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SqlDataProvider.Repositories
{
    public class UserRepository : Repository<AppUser>, IUserRepository
    {
        private readonly UserManager<AppUser, int> _userManager;

        public UserRepository(ApplicationDbContext context) : base(context)
        {
            _userManager = new UserManager<AppUser, int>(new UserStore<AppUser, Role, int, Login, UserRole, Claim>(context));
        }

        public async Task<IdentityResult> RegisterUser(AppUser user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            return result;
        }

        public async Task<AppUser> FindUser(string userName, string password)
        {
            AppUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public async Task<AppUser> FindUser(string userName)
        {
            AppUser user = await _userManager.FindByNameAsync(userName);

            return user;
        }

        public void Dispose()
        {
            _userManager.Dispose();
        }
    }
}