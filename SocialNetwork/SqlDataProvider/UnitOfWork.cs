﻿using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;
using DatabaseProvider;
using DatabaseProvider.Entity;
using DatabaseProvider.Identity;
using DatabaseProvider.Repository;
using Microsoft.AspNet.Identity.EntityFramework;
using SqlDataProvider.Repositories;
using ApplicationRoleManager = SqlDataProvider.Identity.ApplicationRoleManager;
using ApplicationUserManager = SqlDataProvider.Identity.ApplicationUserManager;

namespace SqlDataProvider
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Private Fields

        private readonly ApplicationDbContext _context;
        private IRepository<AppUser> _userRepository;
        private IUserManager<AppUser, int> _userManager;
        private IRoleManager<Role, int> _roleManager;
        private IRepository<UserDetails> _userDetailsRepository;
        private IRepository<Message> _messageRepository;
        private IRepository<Chat> _chatRepository;
        private IRepository<Role> _roleRepository;
        private IRepository<ChatHistory> _chatHistoryRepository;

        #endregion
        
        public UnitOfWork(string connectionString)
        {
            _context = new ApplicationDbContext(connectionString);
        }

        #region IUnitOfWork Members

        public IUserManager<AppUser, int> UserManager
            => _userManager ?? (_userManager = new ApplicationUserManager(new UserStore<AppUser, Role, int, Login, UserRole, Claim>(_context)));

        public IRoleManager<Role, int> RoleManager 
            => _roleManager ?? (_roleManager = new ApplicationRoleManager(new RoleStore<Role, int, UserRole>(_context)));

        public IRepository<AppUser> UserRepository 
            => _userRepository ?? (_userRepository = new Repository<AppUser>(_context));

        public IRepository<UserDetails> UserDetailsRepository 
            => _userDetailsRepository ?? (_userDetailsRepository = new Repository<UserDetails>(_context));

        public IRepository<Message> MessageRepository 
            => _messageRepository ?? (_messageRepository = new Repository<Message>(_context));

        public IRepository<Chat> ChatRepository 
            => _chatRepository ?? (_chatRepository = new Repository<Chat>(_context));

        public IRepository<Role> RoleRepository 
            => _roleRepository ?? (_roleRepository = new Repository<Role>(_context));

        public IRepository<ChatHistory> ChatHistoryRepository 
            => _chatHistoryRepository ?? (_chatHistoryRepository = new Repository<ChatHistory>(_context));

        public DbContextTransaction BeginTransaction()
        {
            return _context.Database.BeginTransaction();
        }
        public int SaveChanges()
        {
            return _context.SaveChanges(); 
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            _userRepository = null;
            _messageRepository = null;
            _chatRepository = null;
            _roleRepository = null;
            _userRepository = null;
            _context.Dispose();
        }

        #endregion
    }
}