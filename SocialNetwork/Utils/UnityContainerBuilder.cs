﻿using BLL.Interfaces;
using BLL.Services;
using DatabaseProvider;
using Microsoft.Practices.Unity;
using SqlDataProvider;

namespace Utils
{
    public class UnityContainerBuilder
    {
        public static IUnityContainer BuildUnityContainer(string connectionString)
        {
            var container = new UnityContainer();
            container.RegisterType<IUnitOfWork, UnitOfWork>(new ContainerControlledLifetimeManager(),
                new InjectionConstructor(connectionString));
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IChatService, ChatService>();
            container.RegisterType<IMessageService, MessageService>();
            return container;
        }
    }
}